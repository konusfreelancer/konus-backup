#!/usr/bin/python2
import os
import os.path
import time
import subprocess
import shlex
import shutil
import logging
import datetime

timestamp = lambda: time.strftime('%H.%M-%d.%b.%Y')

script_path = os.path.dirname(os.path.abspath(__file__))

from conf import CONFIG

now = datetime.datetime.now()

year, week, day = datetime.date.isocalendar(now)
CONFIG['date'] = {'year': year, 'week': week, 'day': day}

date_path = lambda config: str(config['date']['year']) + '-' + str(config['date']['week']).rjust(2, '0') + '/' + str(config['date']['day'])

class MysqlDumper(object):
    def __init__(self, cfg_name):
        self.username = CONFIG[cfg_name].get('username')
        self.password = CONFIG[cfg_name].get('password')
        self.hostname = CONFIG[cfg_name].get('hostname', 'localhost')
        self.dump_path = os.path.abspath(CONFIG.get('dump_path').rstrip('/')) + '/' + date_path(CONFIG) + '/mysql'
        self.skip_databases = filter(bool, CONFIG[cfg_name].get('skip_databases') or [])
        try:
            os.makedirs(self.dump_path)
        except OSError:
            pass

    def get_databases(self):
        cmd_str = "mysql -u {username} -p{password} -h {hostname} --silent -N -e 'SHOW DATABASES'".format(username=self.username, password=self.password, hostname=self.hostname)
        p = subprocess.Popen(shlex.split(cmd_str), stdout=subprocess.PIPE)
        mysql_dbs = [db.strip() for db in p.stdout.readlines()]
        return [db for db in mysql_dbs if db not in self.skip_databases]

    def dump(self, database):
        filename = os.path.join(self.dump_path, "mysql-{db}.sql".format(db=database))
        dump_cmd = "mysqldump -u {username} -p{password} -h {hostname} -e --opt -c {db} > {filename}".format(username=self.username, password=self.password, hostname=self.hostname, db=database, filename=filename)
        os.system(dump_cmd)
        compress_cmd = "nice -n 10 bzip2 -c {filename} > {filename}.bz2".format(filename=filename)
        os.system(compress_cmd)
        os.remove(filename)
        return filename + '.bz2'

    def dump_all(self):
        databases = self.get_databases()
        print "Databases found: {0}".format(", ".join(databases))
        print "Skipping databases: {0}".format(", ".join(self.skip_databases))
        filenames = []
        for database in databases:
            print "Starting `{0}` database dump".format(database)
            filename = mysql_dumper.dump(database)
            size = os.stat(filename).st_size
            print "Dump created: {0} [{1} bytes]".format(filename, size)
            filenames.append(filename)
        return filenames

class FSDumper(object):
    def __init__(self, cfg_name):
        self.dump_root = os.path.abspath(CONFIG.get('dump_path').rstrip('/'))
        self.dump_path = self.dump_root + '/' + date_path(CONFIG)
        self.meta_path = self.dump_path + '/meta'
        self.paths = CONFIG[cfg_name].get('paths') or []
        try:
            #os.makedirs(self.dump_path)
            os.makedirs(self.meta_path)
        except OSError:
            pass

    def dump(self, path):
        today_meta = self.meta_path + '/' + path.replace('/', '') + '.meta'
        noslash_path = path.replace('/', '')
        if day > 1:
            prev_meta = self.dump_path[:-1] + str(day - 1) + '/meta/' + noslash_path + '.meta'
            try:
                print("Previous meta: {path}".format(path=prev_meta))
                print("Today meta: {path}".format(path=today_meta))
                shutil.copy(prev_meta, today_meta)
            except IOError:
                print "Failed to copy previous increment metafile. Continuing with full backup."
        filename = os.path.join(self.dump_path, "{path}.tar.gz".format(path=noslash_path))
        print "Dumping %s to %s\n" % (path, filename)
        os.system('''nice -n 10 /bin/tar --create --ignore-failed-read --one-file-system --preserve-permissions --recursion --sparse --gzip --file="%s" --listed-incremental="%s" "%s"''' % (filename, today_meta, path))
        return filename

    def dump_all(self):
        filenames = []
        for path in self.paths:
            path = path['path']
            filename = self.dump(path)
            if not filename:
                print "Dump failed. File was not created."
            else:
                size = os.stat(filename).st_size
                print "Dump created: {name} [{size} bytes]".format(name=filename, size=size)
                filenames.append(filename)
        return filenames


if __name__ == '__main__':
    print "Backup started."
    # Dump MySQL databases
    print "Dumping MySQL databases"
    mysql_dumper = MysqlDumper('mysql')
    mysql_dump_files = mysql_dumper.dump_all()
    print "Dumping Paths"
    fs_dumper = FSDumper('paths')
    fs_dumper.dump_all()
    print "Dumping finished."
    print "Uploading to S3"

    dump_path = os.path.abspath(CONFIG['dump_path'].rstrip('/')) + '/' + date_path(CONFIG)
    os.system("""nice -n 15 s3cmd --recursive -c {script_path}/s3.conf put {dump_path}/ {s3bucket}{date}/""".format(script_path=script_path, dump_path=dump_path, s3bucket=CONFIG['s3bucket'], date=date_path(CONFIG)))
    print "That's all folks."

